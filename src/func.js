const getSum = (str1, str2) => {
   if(typeof str1 != "string" || typeof str2 != "string"){
    return false;
  }
  if(str1.match(/[^0-9]/) || str2.match(/[^0-9]/)){
    return false;
  }  
  if(str1.length==0){
    return str2;
  }
  if(str2.length==0){
    return str1;
  }
  let mass1 = Array.from(str1).reverse();
  let mass2 = Array.from(str2).reverse();
  if(mass1.length>=mass2.length){
    for(let i=0;i<mass2.length;i++){
       mass1[i] = Number.parseInt(mass1[i]);
       mass2[i] = Number.parseInt(mass2[i]);
       mass1[i]+=mass2[i]
       if(mass1[i]>9){
         mass1[i]%=2;
         if(i+1<mass1.length){
          mass1[i+1]+=1;
         }
         else{
           mass1[i]+=1;
         }
       }
    }
    return mass1.reverse().join('');
  }
  else{
    for(let i=0;i<mass1.length;i++){
      mass1[i] = Number.parseInt(mass1[i]);
      mass2[i] = Number.parseInt(mass2[i]);
      mass2[i]+=mass1[i]
      if(mass2[i]>9){
        mass2[i]%=2;
        if(i+1<mass2.length){
         mass2[i+1]+=1;
        }
        else{
          mass2[i]+=1;
        }
      }
   }
   return mass2.reverse().join('');
  }
 
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
 let lt = Array.from(listOfPosts);
  let count = 0;
  let post=0;
  lt.forEach(ps=>{
    if(ps.author==authorName){
      post++;
    }
     if(ps.hasOwnProperty('comments')){
       
       let tt = Array.from(ps.comments)
       tt.forEach(el=>{
         if(el.author==authorName){
           count++;
         }
       })
      
     }
  })
  return "Post:"+post+",comments:"+count;
};

const tickets=(people)=> {
  let sum = 0;
  let mass = Array.from(people);
  if(mass[0]>25){
    return "NO";
  }
  for(let i=0;i<mass.length-1;i++){
    sum+=Number.parseInt(mass[i]);
  }
  return sum>mass[mass.length-1]?"YES":"NO";
};




module.exports = {getSum, getQuantityPostsByAuthor, tickets};
